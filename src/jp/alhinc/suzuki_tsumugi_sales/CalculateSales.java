package jp.alhinc.suzuki_tsumugi_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.TreeSet;
public class CalculateSales {

	public static void main(String[]args){
		// 支店定義ファイルの読み取り
		HashMap<String, String> nameMap = new HashMap<>();			//キー(支店コード)と値(支店名)
		HashMap<String, Long> salesMap = new HashMap<>();			//キー(支店コード)値（売上）

		try{
			BufferedReader br = null;
			try {

				File blanchFile = new File(args[0],"branch.lst");
				//支店定義ファイルが存在するか否か
				if(blanchFile.exists() == false){
					System.out.println("支店定義ファイルが存在しません");
					return;
				}//①File オブジェクトを生成							 //引数1にファイルがある場所、引数2にファイル名の指定

				//②FileReaderオブジェクトを生成//fileReaderのインスタンス	   //③FIleReaderオブジェクトを引数として、そこから文字列を受け取る//BufferedReaderオブジェクトの生成
				br = new BufferedReader(new FileReader(blanchFile));

				String line;																//lineオブジェクトを生成
				while((line = br.readLine()) != null){
					//lineがnullになるまで実行.

					String[] branchList = line.split(",", -1);	//文を分割し、それぞれを配列の要素として格納,

					if(!branchList[0].matches("\\d{3}") || branchList.length != 2){
						System.out.println("支店定義ファイルのフォーマットが不正です");
						return;
					}

					nameMap.put(branchList[0], branchList[1]);
					salesMap.put(branchList[0], 0L);										//salesMapに入った時点で数になる
				}
			}finally{
				br.close();													// closeメソッドでストリームを閉じる。
			}

			FilenameFilter filter = new FilenameFilter() {									//コマンドライン引数で指定されたディレクトリから拡張子がrcd、且つファイル名が数字8桁のファイルを検索

				public boolean accept(File dir, String filename){ //指定されたファイルをファイルリストに含めるか								//拡張子の指定　かつ　ファイル名が数字8桁でフィルタする
					return filename.matches("^\\d{8}.rcd$") && new File(dir,filename).isFile();
				}
			};


			File[] files = new File(args[0]).listFiles(filter); //フィルタを通ったファイルリストは変数files

			ArrayList<Integer> rcdNumbers = new ArrayList<Integer>(); //intにする

			for(int i=0; i<files.length; i++) {
				int fileNameNumber = Integer.parseInt(files[i].getName().substring(0,8));//文字列から数字へ
				rcdNumbers.add(fileNameNumber);;
			}
			Collections.sort(rcdNumbers);//listをソート

			//配列の数字を比較
			for(int i=0; i < files.length -1; i++){
				if(rcdNumbers.get(i + 1) - rcdNumbers.get(i) != 1){
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}
			}

			// フィルタ通過後の売上ファイルの読み取り
			for(int i =0; i < files.length; i++){		//File file =new File(args[0],files[i]);//fileオブジェクトは既に生成してる
				try{
					br = new BufferedReader(new FileReader(files[i]));

					String line1 = br.readLine();				//1行ずつのlineの読み取り
					String line2 = br.readLine();				//1行ずつのlineの読み取り
					long s = Long.parseLong(line2);//line2をlong型sへ変更(売上数値化)
					String line3 = br.readLine();

					if (!salesMap.containsKey(line1)) {
						System.out.println(files[i].getName()+"の支店コードが不正です");
						return;
					}

					Long total1 = s + salesMap.get(line1); //初期値は0
					salesMap.put(line1, total1);//salessalesMapに入れる

					if(line3 != null){
						System.out.println(files[i].getName() + "のフォーマットが不正です");
						return;
					}

					if(total1.toString().length() > 10){
						System.out.println("合計金額が10桁を超えました");
						return;
					}
				}finally{
					br.close();
				}
			}
			// 	 	以下集計結果出力
			// 	    支店別集計ファイルへの出力
			BufferedWriter bw = new BufferedWriter(new FileWriter(new File(args[0],"branch.out")));
			try{
				for(String code : new TreeSet<>(nameMap.keySet())){//拡張for文			//支店の数分繰り返す
					bw.write(code+","+nameMap.get(code)+","+salesMap.get(code));	//支店別集計ファイルへの出力
					bw.newLine();												//改行
				}
				//以下エラー処理
			}finally{
				bw.close();
			}
		}catch(Exception e){
			System.out.println("予期せぬエラーが発生しました");
		}
	}
}
